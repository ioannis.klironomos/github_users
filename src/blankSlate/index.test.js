import React from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import BlankSlate from './index';

Enzyme.configure({adapter: new Adapter()});

const setup = (children = null) => {
  let props = {
    message: 'This page is empty',
    mainAction: jest.fn(),
    mainActionLabel: 'Do that'
  };

  if (children) {
    props = {
      ...props,
      children
    };
  }

  const enzymeWrapper = mount(<BlankSlate {...props} />);

  return {
    props,
    enzymeWrapper
  };
};

describe('components', () => {
  describe('BlankSlate', () => {
    it('should render self and subcomponents', () => {
      const {enzymeWrapper, props} = setup();

      expect(enzymeWrapper.find('p').first().text()).toBe('This page is empty');

      const mainButtonProps = enzymeWrapper.find('Button').first().props();
      expect(mainButtonProps.onClick).toBe(props.mainAction);
      expect(mainButtonProps.children).toBe('Do that');

      const rowProps = enzymeWrapper.find('Row').last().props();
      expect(rowProps.children).toBeUndefined();
    });

    it('should render children', () => {
      const {enzymeWrapper, props} = setup('some children');

      const rowProps = enzymeWrapper.find('Row').last().props();
      expect(rowProps.children).toBe('some children');
    });

    it('should call main action on mainButton click', () => {
      const {enzymeWrapper, props} = setup();
      expect(props.mainAction.mock.calls.length).toBe(0);

      const mainButton = enzymeWrapper.find('Button').last();
      mainButton.props().onClick();
      expect(props.mainAction.mock.calls.length).toBe(1);
    });
  });
});
