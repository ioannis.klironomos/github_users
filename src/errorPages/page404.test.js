import React from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {StaticRouter} from 'react-router';

import Page404 from './page404';

Enzyme.configure({adapter: new Adapter()});

const setup = (children = null) => {
  let props = {};

  if (children) {
    props = {
      ...props,
      children
    };
  }

  const enzymeWrapper = mount(
    <StaticRouter location='someLocation' context={{}}>
      <Page404 {...props} />
    </StaticRouter>
  );

  return {
    props,
    enzymeWrapper
  };
};

describe('components', () => {
  describe('Page404', () => {
    it('should render self and subcomponents', () => {
      const {enzymeWrapper, props} = setup();

      expect(enzymeWrapper.find('p').first().text())
        .toBe('The page you are looking for does not exist.');

      const mainLinkProps = enzymeWrapper.find('Link').first().props();
      expect(mainLinkProps.to).toBe('/');

      const rowProps = enzymeWrapper.find('Row').last().props();
      expect(rowProps.children).toBeUndefined();
    });

    it('should render children', () => {
      const {enzymeWrapper, props} = setup('some children');

      const rowProps = enzymeWrapper.find('Row').last().props();
      expect(rowProps.children).toBe('some children');
    });
  });
});
