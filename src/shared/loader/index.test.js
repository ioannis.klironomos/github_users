import React from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {StaticRouter} from 'react-router';

import Loader from './index';

Enzyme.configure({adapter: new Adapter()});

const setup = () => {
  return mount(
    <StaticRouter location='someLocation' context={{}}>
      <Loader />
    </StaticRouter>
  );
};

describe('components', () => {
  describe('shared', () => {
    describe('Loader', () => {
      it('should render self', () => {
        const enzymeWrapper = setup();

        expect(enzymeWrapper.exists()).toBe(true);
        expect(enzymeWrapper.find('div.loader').exists()).toBe(true);
      });
    });
  });
});
