import React, {createElement} from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Scroller from './index';

Enzyme.configure({adapter: new Adapter()});

const scrollMock = jest.fn(() => 'scroll');
const setup = username => {
  const props = {
    children: <div id='component' />,
    username
  };

  const enzymeWrapper = mount(<Scroller {...props} />);
  const element = enzymeWrapper.find(`#${username}`).first();
  document.getElementById = jest.fn(() => element);
  if (element.length) {
    element.scrollIntoView = scrollMock;
  }

  return {
    props,
    enzymeWrapper,
    element
  };
};

describe('components', () => {
  describe('shared', () => {
    describe('Scroller', () => {
      it('should render self and subcomponents', () => {
        const {enzymeWrapper, element} = setup('component');

        expect(enzymeWrapper.exists()).toBe(true);
        expect(element.exists()).toBe(true);
      });

      it('should scroll to element if found', () => {
        const {enzymeWrapper, props, element} = setup('component');

        expect(element.scrollIntoView.mock.calls.length).toBe(1);
      });

      it('should not scroll if no element found', () => {
        const {enzymeWrapper, props, element} = setup('component2');

        expect(element.scrollIntoView).toBeUndefined;
      });
    });
  });
});
