import {Component} from 'react';
import PropTypes from 'prop-types';

class Scroller extends Component {

  componentDidMount() {
    const {username, scrollOptions} = this.props;
    if (!username) {
      return;
    }

    const element = document.getElementById(username);
    if (element) {
      element.scrollIntoView(scrollOptions);
    }
  }

  render() {
    return this.props.children;
  }
}

Scroller.propTypes = {
  username: PropTypes.string,
  scrollOptions: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.array,
    PropTypes.string
  ]).isRequired
};

Scroller.defaultProps = {
  username: '',
  scrollOptions: {}
};

export default Scroller;
