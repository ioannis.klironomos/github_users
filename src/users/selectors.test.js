import {fromJS} from 'immutable';
import {
  selectIsFetching,
  selectUsernames,
  selectUsersByUsername,
  selectLastUserId,
  selectUsersError,
  selectLastViewedUsername,
  selectMightHaveMoreUsers
} from './selectors';

const mockUserState = fromJS({
  users: {
    isFetching: true,
    canFetchMoreUsers: false,
    lastViewedUsername: 'c',
    usernames: ['a', 'b'],
    byUsername: {
      a: {
        details: {
          id: 1,
          login: 'a',
          avatar_url: ''
        }
      },
      b: {
        details: {
          id: 2,
          login: 'b',
          avatar_url: ''
        }
      }
    },
    error: 'some error'
  }
});

describe('selectors', () => {
  describe('User', () => {

    describe('selectIsFetching', () => {
      it('should return isFetching bool', () => {
        const selected = selectIsFetching(mockUserState);
        expect(selected).toBe(true);
      });
    });

    describe('selectUsernames', () => {
      it('should return immutable list of usernames', () => {
        const selected = selectUsernames(mockUserState);
        expect(selected).toEqual(fromJS(['a', 'b']));
      });
    });

    describe('selectUsersByUsername', () => {
      it('should return immutable map of users byUsername', () => {
        const selected = selectUsersByUsername(mockUserState);
        expect(selected).toEqual(fromJS({
          a: {
            details: {
              id: 1,
              login: 'a',
              avatar_url: ''
            }
          },
          b: {
            details: {
              id: 2,
              login: 'b',
              avatar_url: ''
            }
          }
        }));
      });
    });

    describe('selectLastUserId', () => {
      it('should return the last user\'s id', () => {
        const selected = selectLastUserId(mockUserState);
        expect(selected).toBe(2);
      });
    });

    describe('selectUsersError', () => {
      it('should return the error', () => {
        const selected = selectUsersError(mockUserState);
        expect(selected).toBe('some error');
      });
    });

    describe('selectLastViewedUsername', () => {
      it('should return the last viewed username', () => {
        const selected = selectLastViewedUsername(mockUserState);
        expect(selected).toBe('c');
      });
    });

    describe('selectMightHaveMoreUsers', () => {
      it('should return if there maybe are more users to fetch', () => {
        const selected = selectMightHaveMoreUsers(mockUserState);
        expect(selected).toBe(false);
      });
    });
  });
});
