import {fromJS} from 'immutable';
import actionTypes from './constants';
import userReducer, {initialState as userInitialState} from '../user/reducer';
import userActionTypes from '../user/constants';

const initialState = {
  isFetching: true,
  byUsername: {},
  usernames: [],
  error: null,
  lastViewedUsername: null,
  canFetchMoreUsers: true
};

const delegateActionToUser = (state, username, type, payload) => {
  const userState = username
    ? state.getIn(['byUsername', username])
    : fromJS(userInitialState());

  return state.setIn(['byUsername', username], userReducer(userState, {type, payload}));
};

const updateUsernames = (state, username) => {
  const usernames = state.get('usernames');
  if (usernames.includes(username)) {
    return state;
  }

  return state.set('usernames', usernames.push(username));
};

const toggleCanFetchMoreUsers = state => {
  return state.set('canFetchMoreUsers', !state.get('canFetchMoreUsers'));
};

const setUsers = (state, data = []) => {
  if (!data || !data.length) {
    return toggleCanFetchMoreUsers(state);
  }

  return state.withMutations(map => {
    // Remove error if any
    map.set('error', null);

    data.forEach(user => {
      const {login} = user;
      if (!login) {
        return;
      }

      delegateActionToUser(map, login, userActionTypes.SET_USER, user);
      updateUsernames(map, login);
    });
  });
};

const updateUser = (state, type, username, data = {}) =>
  delegateActionToUser(state, username, type, data);

export default (state = fromJS(initialState), {type, payload = {}} = {}) => {

  switch (type) {
    case actionTypes.START_FETCH_USERS:
      return state
        .set('isFetching', true);

    case actionTypes.FINISH_FETCH_USERS:
      return state
        .set('isFetching', false);

    case actionTypes.SET_USERS_ERROR:
      return state
        .set('error', fromJS(payload.error));

    case actionTypes.DELETE_USERS_ERROR:
      return state
        .set('error', null);

    case actionTypes.SET_USERS:
      return setUsers(state, payload.data);

    case actionTypes.TOGGLE_CAN_FETCH_MORE_USERS:
      return toggleCanFetchMoreUsers(state);

    case actionTypes.SET_LAST_VIEWED_USERNAME:
      return state
        .set('lastViewedUsername', payload.username);

    case userActionTypes.SET_USER:
    case userActionTypes.START_FETCH_USER:
    case userActionTypes.FINISH_FETCH_USER:
      return updateUser(state, type, payload.username, payload.data);

    case userActionTypes.SET_USER_ERROR:
      return updateUser(state, type, payload.username, payload.error);

    default:
      return state;
  }
};
