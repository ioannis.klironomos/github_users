import React from 'react';
import {fromJS} from 'immutable';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import thunkMiddleware from 'redux-thunk';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {Users} from './index';

Enzyme.configure({adapter: new Adapter()});

const mockStore = configureStore([thunkMiddleware]);
const initialState = fromJS({
  users: {
    usernames: ['a', 'b'],
    byUsername: {
      a: {
        details: {
          login: 'a',
          avatar_url: ''
        }
      },
      b: {
        details: {
          login: 'b',
          avatar_url: ''
        }
      }
    }
  }
});
const store = mockStore(initialState);

const scrollMock = jest.fn(() => 'scroll');
const setup = (props = {}) => {
  const enzymeWrapper = mount(
    <Provider store={store}>
      <Users {...props} />
    </Provider>
  );
  const usersElement = enzymeWrapper.find('Users').first();
  usersElement.getData = jest.fn(() => scrollMock)

  return {
    props,
    enzymeWrapper
  };
};

describe('components', () => {
  describe('Users', () => {
    it('should render self with subcomponents', () => {
      const props = {
        usernames: fromJS(['a', 'b']),
        isDataLoaded: true,
        getData: jest.fn(),
        mightHaveMore: true
      };
      const {enzymeWrapper} = setup(props);

      expect(enzymeWrapper.find('UserTeaser')).toHaveLength(2);
      expect(enzymeWrapper.find('PanelHeading').first().text()).toBe('a');
      expect(enzymeWrapper.find('PanelHeading').last().text()).toBe('b');

      expect(enzymeWrapper.find('ButtonComponent').last().text())
        .toBe('Load more users');
    });

    it('should render a blank slate for empty users with data loaded', () => {
      const props = {
        usernames: fromJS([]),
        isDataLoaded: true
      };
      const {enzymeWrapper} = setup(props);

      expect(enzymeWrapper.find('BlankSlate').exists()).toBe(true);
    });

    it('should render error', () => {
      const props = {
        usernames: fromJS(['a', 'b']),
        error: 'some weird error',
        isDataLoaded: true
      };
      const {enzymeWrapper} = setup(props);

      expect(enzymeWrapper.find('Alert').text()).toContain('some weird error');
    });
  });
});
