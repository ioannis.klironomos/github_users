import {createSelector} from 'reselect';

const usersState = state => state.get('users');

export const selectIsFetching = createSelector(
  usersState,
  state => state.get('isFetching')
);

export const selectUsernames = createSelector(
  usersState,
  state => state.get('usernames')
);

export const selectUsersByUsername = createSelector(
  usersState,
  state => state.get('byUsername')
);

export const selectLastUserId = createSelector(
  selectUsernames,
  selectUsersByUsername,
  (usernames, state) => {
    const username = usernames.last();
    return state.getIn([username, 'details', 'id']);
  }
);

export const selectUsersError = createSelector(
  usersState,
  state => state.get('error')
);

export const selectLastViewedUsername = createSelector(
  usersState,
  state => state.get('lastViewedUsername')
);

export const selectMightHaveMoreUsers = createSelector(
  usersState,
  state => state.get('canFetchMoreUsers')
);
