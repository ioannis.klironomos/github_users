import request from 'superagent';
import {USERS_URL, CONTENT_TYPE, ACCEPT} from './constants';

export const fetchUsers = since => {
  const params = since ? {since} : {};

  return request
    .get(USERS_URL)
    .type(CONTENT_TYPE)
    .accept(ACCEPT)
    .query(params);
}
