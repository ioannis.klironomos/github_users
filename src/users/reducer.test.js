import {fromJS} from 'immutable';
import reducer from './reducer';
import actionTypes from './constants';

// SET_USERS
// TOGGLE_CAN_FETCH_MORE_USERS
// SET_LAST_VIEWED_USERNAME

describe('reducers', () => {
  describe('Users', () => {
    describe('INIT', () => {
      it('should return the initial state', () => {
        expect(reducer(undefined).toJS()).toEqual({
          isFetching: true,
          byUsername: {},
          usernames: [],
          error: null,
          lastViewedUsername: null,
          canFetchMoreUsers: true
        });
      });
    });

    describe('SET_USERS', () => {
      it('should set the users data', () => {
        const action = {
          type: actionTypes.SET_USERS,
          payload: {
            data: [{
              login: 'user1',
              bb: 'ccc'
            }]
          }
        };

        expect(reducer(undefined, action).toJS()).toEqual({
          isFetching: true,
          byUsername: {
            user1: {
              details: {
                id: null,
                login: 'user1',
                avatar_url: 'http://via.placeholder.com/150x150',
                html_url: null,
                bb: 'ccc'
              },
              isDataLoaded: true,
              error: null
            }
          },
          usernames: ['user1'],
          error: null,
          lastViewedUsername: null,
          canFetchMoreUsers: true
        });

        const novykhAction = {
          type: actionTypes.SET_USERS,
          payload: {
            data: [{
              id: 601483,
              login: 'novykh',
              avatar_url: 'https://avatars0.githubusercontent.com/u/601483?v=4',
              html_url: 'https://github.com/novykh'
            }]
          }
        };

        expect(reducer(undefined, novykhAction).toJS()).toEqual({
          isFetching: true,
          byUsername: {
            novykh: {
              details: {
                avatar_url: 'https://avatars0.githubusercontent.com/u/601483?v=4',
                html_url: 'https://github.com/novykh',
                id: 601483,
                login: 'novykh'
              },
              error: null,
              isDataLoaded: true
            }
          },
          usernames: ['novykh'],
          error: null,
          lastViewedUsername: null,
          canFetchMoreUsers: true
        });
      });

      it('should toggle canFetchMoreUsers when no data', () => {
        const action = {
          type: actionTypes.SET_USERS,
          payload: {
            data: []
          }
        };

        expect(reducer(undefined, action).toJS()).toEqual({
          isFetching: true,
          byUsername: {},
          usernames: [],
          error: null,
          lastViewedUsername: null,
          canFetchMoreUsers: false
        });
      });
    });

    describe('START_FETCH_USERS', () => {
      it('should update the isFetching to true', () => {
        const action = {
          type: actionTypes.START_FETCH_USERS
        };

        const expected = {
          isFetching: true,
          byUsername: {},
          usernames: [],
          error: null,
          lastViewedUsername: null,
          canFetchMoreUsers: true
        };

        expect(reducer(undefined, action).toJS()).toEqual(expected);

        const newState = fromJS({
          isFetching: false,
          byUsername: {},
          usernames: [],
          error: null,
          lastViewedUsername: null,
          canFetchMoreUsers: true
        });
        expect(reducer(newState, action).toJS()).toEqual(expected);
      });
    });

    describe('FINISH_FETCH_USERS', () => {
      it('should update the isFetching to false', () => {
        const action = {
          type: actionTypes.FINISH_FETCH_USERS
        };

        expect(reducer(undefined, action).toJS()).toEqual({
          isFetching: false,
          byUsername: {},
          usernames: [],
          error: null,
          lastViewedUsername: null,
          canFetchMoreUsers: true
        });
      });
    });

    describe('SET_USERS_ERROR', () => {
      it('should set the error', () => {
        const action = {
          type: actionTypes.SET_USERS_ERROR,
          payload: {
            error: 'some error'
          }
        };

        expect(reducer(undefined, action).toJS()).toEqual({
          isFetching: true,
          byUsername: {},
          usernames: [],
          error: 'some error',
          lastViewedUsername: null,
          canFetchMoreUsers: true
        });
      });
    });

    describe('DELETE_USERS_ERROR', () => {
      it('should set the error', () => {
        const action = {
          type: actionTypes.DELETE_USERS_ERROR
        };

        const state = fromJS({
          isFetching: true,
          byUsername: {},
          usernames: [],
          error: 'some error',
          lastViewedUsername: null,
          canFetchMoreUsers: true
        });

        expect(reducer(undefined, action).toJS()).toEqual({
          isFetching: true,
          byUsername: {},
          usernames: [],
          error: null,
          lastViewedUsername: null,
          canFetchMoreUsers: true
        });
      });
    });

    describe('TOGGLE_CAN_FETCH_MORE_USERS', () => {
      it('should toggle canFetchMoreUsers', () => {
        const action = {
          type: actionTypes.TOGGLE_CAN_FETCH_MORE_USERS
        };

        expect(reducer(undefined, action).toJS()).toEqual({
          isFetching: true,
          byUsername: {},
          usernames: [],
          error: null,
          lastViewedUsername: null,
          canFetchMoreUsers: false
        });
      });
    });

    describe('SET_LAST_VIEWED_USERNAME', () => {
      it('should set a username as last seen', () => {
        const action = {
          type: actionTypes.SET_LAST_VIEWED_USERNAME,
          payload: {
            username: 'a'
          }
        };

        expect(reducer(undefined, action).toJS()).toEqual({
          isFetching: true,
          byUsername: {},
          usernames: [],
          error: null,
          lastViewedUsername: 'a',
          canFetchMoreUsers: true
        });
      });
    });
  });
});
