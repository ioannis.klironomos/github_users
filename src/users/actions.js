import createAction from '../helpers/actionCreator';
import actionTypes, {DEFAULT_ERROR} from './constants';
import {selectLastUserId} from './selectors';
import {fetchUsers} from './api';

const startFetching = () =>
  createAction(actionTypes.START_FETCH_USERS);

const finishFetching = () =>
  createAction(actionTypes.FINISH_FETCH_USERS);

const setError = error =>
  createAction(actionTypes.SET_USERS_ERROR, {error});

const setUsers = data =>
  createAction(actionTypes.SET_USERS, {data});

const toggleCanFetchMoreUsers = () =>
  createAction(actionTypes.TOGGLE_CAN_FETCH_MORE_USERS);

export const getUsers = () => {
  return (dispatch, getState) => {
    dispatch(startFetching());

    const lastId = selectLastUserId(getState());
    return fetchUsers(lastId)
      .then(
        ({body = []} = {}) => {
          if (!body.length) {
            // In the weird situation where we reach the end at some point
            // or no users are being returned
            // then hide the `load more button` (in reducer)
            // and show it again (dispatch) after 5 sec
            setTimeout(() => dispatch(toggleCanFetchMoreUsers()), 5000);
          }
          dispatch(setUsers(body));
        },
        ({message = DEFAULT_ERROR} = {}) => dispatch(setError(message))
      )
      .then(() => dispatch(finishFetching()));
  };
};

export const deleteError = () => createAction(actionTypes.DELETE_USERS_ERROR);

export const setLastViewedUsername = username =>
  createAction(actionTypes.SET_LAST_VIEWED_USERNAME, {username});
