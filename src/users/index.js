import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {List as immutableList} from 'immutable';
import {connect} from 'react-redux';
import {getUsers, deleteError} from './actions';
import {
  selectIsFetching,
  selectUsernames,
  selectUsersError,
  selectLastViewedUsername,
  selectMightHaveMoreUsers
} from './selectors';
import debounce from '../helpers/simpleDebounce';
import dataLoader from '../hoc/dataLoader';

import {Row} from 'react-bootstrap';
import Scroller from '../shared/scroller';
import UserTeaser from '../user/teaser';
import Alert from '../alert';
import BlankSlate from '../blankSlate';
import Button from '../button';

export class Users extends Component {

  constructor(props) {
    super(props);

    this.getData = debounce(this.props.getData, 300);
    this.renderFooter = this.renderFooter.bind(this);
  }

  renderFooter() {
    const {usernames, isDataLoaded, error, hideAlert, mightHaveMore} = this.props;
    if (error) {
      return (
        <Alert
          title={'Couldn\'t load more users'}
          message={`Reason: ${error}`}
          mainAction={this.getData}
          mainActionLabel='Try again'
          hideAction={hideAlert}
          hideActionLabel='Hide alert'
        />
      );
    }

    const shouldShowLoadMore = isDataLoaded && !usernames.isEmpty() && mightHaveMore;
    if (shouldShowLoadMore) {
      return (
        <Button onClick={this.getData}>
          Load more users
        </Button>
      );
    }

    return null;
  }

  render() {
    const {
      usernames,
      isDataLoaded,
      lastViewedUsername
    } = this.props;

    if (isDataLoaded && usernames.isEmpty()) {
      return (
        <BlankSlate
          message='There are no users to show.'
          mainAction={this.getData}
          mainActionLabel='Fetch some'
        >
          {this.renderFooter()}
        </BlankSlate>
      );
    }

    return (
      <Scroller username={lastViewedUsername}>
        <Row>
          {usernames.map(username => <UserTeaser key={username} username={username} />)}
        </Row>
        <Row style={{textAlign: 'center'}}>
          {this.renderFooter()}
        </Row>
      </Scroller>
    );
  }
}

Users.propTypes = {
  usernames: PropTypes.instanceOf(immutableList).isRequired,
  isDataLoaded: PropTypes.bool,
  lastViewedUsername: PropTypes.string,
  error: PropTypes.string,
  getData: PropTypes.func,
  hideAlert: PropTypes.func
};

Users.defaultProps = {
  isDataLoaded: true,
  lastViewedUsername: '',
  error: '',
  getData: noop => noop,
  hideAlert: noop => noop
};

const mapStateToProps = state => {
  return {
    isDataLoaded: !selectIsFetching(state),
    usernames: selectUsernames(state),
    error: selectUsersError(state),
    lastViewedUsername: selectLastViewedUsername(state),
    mightHaveMore: selectMightHaveMoreUsers(state)
  };
};

const mapDispatchToProps = {
  getData: getUsers,
  hideAlert: deleteError
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(dataLoader(Users));
