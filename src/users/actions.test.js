import {fromJS} from 'immutable';
import configureMockStore from 'redux-mock-store';
import thunkMiddleware from 'redux-thunk';
import request from 'superagent';
import {getUsers} from './actions';
import actionTypes from './constants';

const middlewares = [thunkMiddleware];
const mockStore = configureMockStore(middlewares);
const store = mockStore(fromJS({
  users: {
    isFetching: true,
    byUsername: {},
    usernames: [],
    error: null,
    lastViewedUsername: null,
    canFetchMoreUsers: true
  }
}));

describe('actions', () => {
  describe('getUsers', () => {
    it('should return action with type and payload', () => {
      spyOn(request.Request.prototype, 'end').and.callFake(cb => {
        cb(null, {
          body: []
        });
      });

      store.dispatch(getUsers()).then(
        data => {
          expect(store.getActions()).toEqual([{
            type: 'START_FETCH_USERS',
            payload: undefined,
          }, {
            type: 'SET_USERS',
            payload: {
              data: []
            }
          }, {
            type: 'FINISH_FETCH_USERS',
            payload: undefined
          }]);
        }
      );
    });
  });
});
