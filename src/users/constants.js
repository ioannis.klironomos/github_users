import keymirror from 'keymirror';

export default keymirror({
  START_FETCH_USERS: null,
  FINISH_FETCH_USERS: null,
  SET_USERS: null,
  SET_USERS_ERROR: null,
  DELETE_USERS_ERROR: null,
  SET_LAST_VIEWED_USERNAME: null,
  TOGGLE_CAN_FETCH_MORE_USERS: null
});

export const DEFAULT_ERROR = 'Something went wrong';
export const USERS_URL = 'https://api.github.com/users';
export const CONTENT_TYPE = 'application/json';
export const ACCEPT = 'application/vnd.github.v3+json';
