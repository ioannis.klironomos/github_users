import {fromJS} from 'immutable';
import actionTypes from './constants';

const DEFAULT_USERNAME = 'Unknown';
const DEFAULT_AVATAR_URL = 'http://via.placeholder.com/150x150';

export const initialState = {
  details: {
    id: null,
    login: DEFAULT_USERNAME,
    avatar_url: DEFAULT_AVATAR_URL,
    html_url: null
  },
  isDataLoaded: false,
  error: null
};

export default (state = fromJS(initialState), {type, payload = {}} = {}) => {

  switch (type) {
    case actionTypes.SET_USER:
      return state
        .mergeIn(['details'], fromJS(payload))
        .set('isDataLoaded', true);

    case actionTypes.START_FETCH_USER:
      return state;

    case actionTypes.FINISH_FETCH_USER:
      return state
        .set('isDataLoaded', true);

    case actionTypes.SET_USER_ERROR:
      return state
        .set('error', fromJS(payload));

    default:
      return state;
  }
};
