import React from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Avatar from './index';

Enzyme.configure({adapter: new Adapter()});

const setup = () => {
  const props = {
    url: '/',
    height: '150px'
  };

  const enzymeWrapper = mount(<Avatar {...props} />);

  return {
    props,
    enzymeWrapper
  };
};

describe('components', () => {
  describe('User', () => {
    describe('Avatar', () => {
      it('should render self and subcomponents', () => {
        const {enzymeWrapper} = setup();

        const imageProps = enzymeWrapper.find('Image').first().props();
        expect(imageProps.src).toBe('/');
        expect(imageProps.alt).toBe('');
        expect(imageProps.style).toEqual({height: '150px'});
      });
    });
  });
});
