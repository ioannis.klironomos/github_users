import React from 'react';
import PropTypes from 'prop-types';
import {Image} from 'react-bootstrap';

function Avatar({url, altText, height}) {
  return (
    <Image src={url} alt={altText} circle style={{height}} />
  );
}

Avatar.propTypes = {
  url: PropTypes.string,
  height: PropTypes.string,
  altText: PropTypes.string
};

Avatar.defaultProps = {
  url: '/',
  height: '150px',
  altText: ''
};

export default Avatar;
