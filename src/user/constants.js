import keymirror from 'keymirror';

export default keymirror({
  START_FETCH_USER: null,
  FINISH_FETCH_USER: null,
  SET_USER: null,
  SET_USER_ERROR: null
});
