import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {push} from 'react-router-redux';
import {makeSelectUser} from '../selectors';
import strictEqual from '../../helpers/strictEqual';
import {setLastViewedUsername} from '../../users/actions';

import {Col, Panel} from 'react-bootstrap';
import Avatar from '../avatar';
import Button from '../../button';

export class UserTeaser extends Component {

  render() {
    const {
      username,
      avatarUrl,
      goToUserDetails
    } = this.props;

    return (
      <Col md={3} sm={6} xs={12} id={username}>
        <Panel style={{textAlign: 'center'}}>
          <Panel.Heading>
            {username}
          </Panel.Heading>
          <Panel.Body>
            <Avatar url={avatarUrl} altText={username} />
          </Panel.Body>
          <Panel.Footer>
            <Button flavor='primary' onClick={goToUserDetails}>
              See more
            </Button>
          </Panel.Footer>
        </Panel>
      </Col>
    );
  }
}

UserTeaser.propTypes = {
  username: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
  goToUserDetails: PropTypes.func.isRequired
};

const makeMapStateToProps = (_, ownProps) => {
  const selectUser = makeSelectUser();

  return state => selectUser(state, {username: ownProps.username});
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    goToUserDetails: () => {
      dispatch(setLastViewedUsername(ownProps.username));
      dispatch(push(`/users/${ownProps.username}`));
    }
  };
};

const connectOptions = {
  areStatePropsEqual: strictEqual
};

export default connect(
  makeMapStateToProps,
  mapDispatchToProps,
  null,
  connectOptions
)(UserTeaser);
