import React from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {UserTeaser} from './index';

Enzyme.configure({adapter: new Adapter()});

const setup = (props = {}) => {
  const enzymeWrapper = mount(<UserTeaser {...props} />);

  return {
    props,
    enzymeWrapper
  };
};

describe('components', () => {
  describe('User', () => {
    describe('UserTeaser', () => {
      it('should render self with subcomponents', () => {
        const props = {
          username: 'username',
          avatarUrl: 'avatarUrl',
          goToUserDetails: jest.fn()
        };
        const {enzymeWrapper} = setup(props);

        expect(enzymeWrapper.find('PanelHeading').text()).toBe('username');
        expect(enzymeWrapper.find('Avatar').first().props().url).toBe('avatarUrl');
        expect(enzymeWrapper.find('ButtonComponent').first().props().onClick)
          .toBe(props.goToUserDetails);
      });
    });
  });
});
