import createAction from '../helpers/actionCreator';
import actionTypes from './constants';
import {DEFAULT_ERROR} from '../users/constants';
import {fetchUser} from './api';

const startFetching = username =>
  createAction(actionTypes.START_FETCH_USER, {username});

const finishFetching = username =>
  createAction(actionTypes.FINISH_FETCH_USER, {username});

const setError = (username, error) =>
  createAction(actionTypes.SET_USER_ERROR, {username, error});

const setUser = (username, data) =>
  createAction(actionTypes.SET_USER, {username, data});

export const getUser = username => {
  return (dispatch, getState) => {
    dispatch(startFetching(username));

    return fetchUser(username)
      .then(
        ({body = {}} = {}) => dispatch(setUser(username, body)),
        ({message = DEFAULT_ERROR} = {}) => dispatch(setError(username, message))
      )
      .then(() => dispatch(finishFetching(username)))
      .catch(error => {
        dispatch(setError(username, error))
        dispatch(finishFetching(username));
      });
  };
};
