import React from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import DetailItem from './item';

Enzyme.configure({adapter: new Adapter()});

const setup = (props = {}) => {
  const enzymeWrapper = mount(
    <table>
      <tbody>
        <DetailItem {...props} />
      </tbody>
    </table>
  );

  return {
    props,
    enzymeWrapper
  };
};

describe('components', () => {
  describe('User', () => {
    describe('DetailItem', () => {
      it('should render label with value', () => {
        const props = {
          label: 'label',
          value: 'value'
        };
        const {enzymeWrapper} = setup(props);

        expect(enzymeWrapper.find('td').first().text()).toBe('label');
        expect(enzymeWrapper.find('td').last().text()).toBe('value');
      });

      it('should render label with external link for value', () => {
        const props = {
          label: 'label',
          value: 'value',
          isUrl: true
        };
        const {enzymeWrapper} = setup(props);

        expect(enzymeWrapper.find('td').first().text()).toBe('label');
        expect(enzymeWrapper.find('a').first().props().target).toBe('_blank');
        expect(enzymeWrapper.find('a').first().text()).toBe('value');
      });
    });
  });
});
