import React from 'react';
import PropTypes from 'prop-types';

const wrapValue = (isUrl, value) => isUrl
  ? <a target='_blank' href={value}>{value}</a>
  : value;

function DetailItem({label, value, isUrl}) {
  if (!label) {
    return null;
  }

  return (
    <tr>
      <td style={{textAlign: 'right'}}>{label}</td>
      <td style={{textAlign: 'left'}}>{wrapValue(isUrl, value)}</td>
    </tr>
  );
}

DetailItem.propTypes = {
  label: PropTypes.string,
  isUrl: PropTypes.bool
};

DetailItem.defaultProps = {
  value: '-',
  isUrl: false
};

export default DetailItem;
