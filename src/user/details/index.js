import React from 'react';
import PropTypes from 'prop-types';

import {Table} from 'react-bootstrap';
import DetailItem from './item';

function Details({id, username, avatarUrl, htmlUrl}) {
  return (
    <Table striped bordered condensed hover>
      <tbody>
        <DetailItem label='id' value={id} />
        <DetailItem label='username' value={username} />
        <DetailItem label='avatar URL' value={avatarUrl} isUrl />
        <DetailItem label='github URL' value={htmlUrl} isUrl />
      </tbody>
    </Table>
  );
}

Details.propTypes = {
  id: PropTypes.number,
  username: PropTypes.string,
  avatarUrl: PropTypes.string,
  htmlUrl: PropTypes.string
};

export default Details;
