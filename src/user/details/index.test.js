import React from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Details from './index';

Enzyme.configure({adapter: new Adapter()});

const setup = (props = {}) => {
  const enzymeWrapper = mount(<Details {...props} />);

  return {
    props,
    enzymeWrapper
  };
};

describe('components', () => {
  describe('User', () => {
    describe('Details', () => {
      it('should render self and subcomponents', () => {
        const props = {
          id: 1,
          username: 'username',
          avatarUrl: 'avatarUrl',
          htmlUrl: 'htmlUrl'
        };
        const {enzymeWrapper} = setup(props);

        expect(enzymeWrapper.find('DetailItem')).toHaveLength(4);
      });
    });
  });
});
