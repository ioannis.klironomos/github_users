import {createSelector} from 'reselect';
import {Map as immutableMap} from 'immutable';
import {selectUsersByUsername} from '../users/selectors';

const getUser = (state, props) => selectUsersByUsername(state).get(props.username)
    || immutableMap.of();

export const makeSelectUser = () => {
  return createSelector(
    getUser,
    state => {
      const {
        details: {
          id,
          login: username,
          avatar_url: avatarUrl,
          html_url: htmlUrl
        } = {},
        isDataLoaded,
        error
      } = state.toJS();

      return {
        isDataLoaded,
        error,
        id,
        username,
        avatarUrl,
        htmlUrl
      };
    }
  );
};
