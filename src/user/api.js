import request from 'superagent';
import {USERS_URL, CONTENT_TYPE, ACCEPT} from '../users/constants';

export const fetchUser = username => {
  if (!username) {
    return Promise.reject('Username does not exist');
  }

  return request
    .get(`${USERS_URL}/${username}`)
    .type(CONTENT_TYPE)
    .accept(ACCEPT);
};
