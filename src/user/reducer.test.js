import {fromJS} from 'immutable';
import reducer from './reducer';
import actionTypes from './constants';

describe('reducers', () => {
  describe('User', () => {
    describe('INIT', () => {
      it('should return the initial state', () => {
        expect(reducer(undefined).toJS()).toEqual({
          details: {
            id: null,
            login: 'Unknown',
            avatar_url: 'http://via.placeholder.com/150x150',
            html_url: null
          },
          isDataLoaded: false,
          error: null
        });
      });
    });

    describe('SET_USER', () => {
      it('should set the user data in details', () => {
        const action = {
          type: actionTypes.SET_USER,
          payload: {
            a: 1,
            bb: 'ccc'
          }
        };

        expect(reducer(undefined, action).toJS()).toEqual({
          details: {
            id: null,
            login: 'Unknown',
            avatar_url: 'http://via.placeholder.com/150x150',
            html_url: null,
            a: 1,
            bb: 'ccc'
          },
          isDataLoaded: true,
          error: null
        });

        const novykhAction = {
          type: actionTypes.SET_USER,
          payload: {
            id: 601483,
            login: 'novykh',
            avatar_url: 'https://avatars0.githubusercontent.com/u/601483?v=4',
            html_url: 'https://github.com/novykh'
          }
        };

        expect(reducer(undefined, novykhAction).toJS()).toEqual({
          details: {
            id: 601483,
            login: 'novykh',
            avatar_url: 'https://avatars0.githubusercontent.com/u/601483?v=4',
            html_url: 'https://github.com/novykh'
          },
          isDataLoaded: true,
          error: null
        });
      });
    });

    describe('START_FETCH_USER', () => {
      it('should return the state', () => {
        const action = {
          type: actionTypes.START_FETCH_USER
        };

        const expected = {
          details: {
            id: null,
            login: 'Unknown',
            avatar_url: 'http://via.placeholder.com/150x150',
            html_url: null
          },
          isDataLoaded: false,
          error: null
        };

        expect(reducer(undefined, action).toJS()).toEqual(expected);

        const newState = fromJS({
          details: {
            id: null,
            login: 'Unknown',
            avatar_url: 'http://via.placeholder.com/150x150',
            html_url: null
          },
          isDataLoaded: true,
          error: null
        });
        expect(reducer(newState, action).toJS()).toEqual(newState.toJS());
      });
    });

    describe('FINISH_FETCH_USER', () => {
      it('should update the dataIsLoaded to true', () => {
        const action = {
          type: actionTypes.FINISH_FETCH_USER
        };

        expect(reducer(undefined, action).toJS()).toEqual({
          details: {
            id: null,
            login: 'Unknown',
            avatar_url: 'http://via.placeholder.com/150x150',
            html_url: null
          },
          isDataLoaded: true,
          error: null
        });
      });
    });

    describe('SET_USER_ERROR', () => {
      it('should set the error', () => {
        const action = {
          type: actionTypes.SET_USER_ERROR,
          payload: 'some error'
        };

        expect(reducer(undefined, action).toJS()).toEqual({
          details: {
            id: null,
            login: 'Unknown',
            avatar_url: 'http://via.placeholder.com/150x150',
            html_url: null
          },
          isDataLoaded: false,
          error: 'some error'
        });
      });
    });
  });
});
