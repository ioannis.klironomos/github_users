import {fromJS} from 'immutable';
import {makeSelectUser} from './selectors';

describe('selectors', () => {
  describe('User', () => {
    describe('makeSelectUser', () => {

      it('should return a selectUser selector', () => {
        const selectUser = makeSelectUser();
        expect(typeof selectUser).toBe('function');
      });

      it('should return user from it\'s selector', () => {
        const mockUserState = fromJS({
          details: {
            id: 601483,
            login: 'novykh',
            avatar_url: 'https://avatars0.githubusercontent.com/u/601483?v=4',
            html_url: 'https://github.com/novykh'
          },
          isDataLoaded: true,
          error: null
        });

        const selectUser = makeSelectUser();
        const selectedUser = selectUser.resultFunc(mockUserState);
        expect(selectedUser).toEqual({
          isDataLoaded: true,
          error: null,
          id: 601483,
          username: 'novykh',
          avatarUrl: 'https://avatars0.githubusercontent.com/u/601483?v=4',
          htmlUrl: 'https://github.com/novykh'
        });
      });
    });
  });
});
