import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {push} from 'react-router-redux';
import {makeSelectUser} from './selectors';
import strictEqual from '../helpers/strictEqual';
import debounce from '../helpers/simpleDebounce';
import {getUser} from './actions';
import dataLoader from '../hoc/dataLoader';

import {Col, Panel} from 'react-bootstrap';
import Alert from '../alert';
import Avatar from './avatar';
import Details from './details';
import Button from '../button';

export class User extends Component {

  constructor(props) {
    super(props);

    this.getData = debounce(this.props.getData, 300);
  }

  render() {
    const {
      id,
      username,
      avatarUrl,
      htmlUrl,
      goToDashboard,
      error
    } = this.props;

    return (
      <Col md={12}>
        <Panel style={{textAlign: 'center'}}>
          <Panel.Heading>
            {username}
          </Panel.Heading>
          <Panel.Body>
            <Avatar url={avatarUrl} altText={username} />
          </Panel.Body>
          <Panel.Body>
            <Details
              id={id}
              username={username}
              avatarUrl={avatarUrl}
              htmlUrl={htmlUrl}
            />
          </Panel.Body>
          <Panel.Footer>
            <Button flavor='primary' onClick={goToDashboard}>
              Go to dashboard
            </Button>
          </Panel.Footer>
        </Panel>

        {error &&
          <Alert
            title={'Couldn\'t fetch the user'}
            message={`Reason: ${error}`}
            mainAction={this.getData}
            mainActionLabel='Try again'
            hideAction={goToDashboard}
            hideActionLabel='Go to dashboard'
          />
        }
      </Col>
    );
  }
}

User.propTypes = {
  id: PropTypes.number,
  username: PropTypes.string,
  avatarUrl: PropTypes.string,
  htmlUrl: PropTypes.string,
  goToDashboard: PropTypes.func,
  error: PropTypes.string
};

User.defaultProps = {
  htmlUrl: '',
  error: ''
};

const makeMapStateToProps = (_, ownProps) => {
  const selectUser = makeSelectUser();
  const username = ownProps.match.params.username;

  return state => selectUser(state, {username});
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    goToDashboard: () => dispatch(push('/')),
    getData: () => dispatch(getUser(ownProps.match.params.username))
  };
};

const connectOptions = {
  areStatePropsEqual: strictEqual
};

export default connect(
  makeMapStateToProps,
  mapDispatchToProps,
  null,
  connectOptions
)(dataLoader(User, {hideWhileLoading: true}));
