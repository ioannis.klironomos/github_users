import configureMockStore from 'redux-mock-store';
import thunkMiddleware from 'redux-thunk';
import request from 'superagent';
import {getUser} from './actions';
import actionTypes from './constants';

const middlewares = [thunkMiddleware];
const mockStore = configureMockStore(middlewares);
const store = mockStore({});

describe('actions', () => {
  describe('getUser', () => {
    it('should return action with type and payload', () => {
      spyOn(request.Request.prototype, 'end').and.callFake(cb => {
        cb(null, {
          body: {}
        });
      });

      store.dispatch(getUser()).then(
        data => {
          expect(store.getActions()).toEqual([{
            type: 'START_FETCH_USER',
            payload: {
              username: undefined
            }
          }, {
            type: 'SET_USER_ERROR',
            payload: {
              error: 'Something went wrong',
              username: undefined
            }
          }, {
            type: 'FINISH_FETCH_USER',
            payload: {
              username: undefined
            }
          }]);
        }
      );
    });
  });
});

