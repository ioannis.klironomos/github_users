import React from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {User} from './index';

Enzyme.configure({adapter: new Adapter()});

const setup = (props = {}) => {
  const enzymeWrapper = mount(<User {...props} />);

  return {
    props,
    enzymeWrapper
  };
};

describe('components', () => {
  describe('User', () => {
    it('should render self with subcomponents', () => {
      const props = {
        id: 1,
        username: 'username',
        avatarUrl: 'avatarUrl',
        htmlUrl: 'htmlUrl',
        goToDashboard: jest.fn(),
      };
      const {enzymeWrapper} = setup(props);

      expect(enzymeWrapper.find('PanelHeading').text()).toBe('username');
      expect(enzymeWrapper.find('Avatar').first().props().url).toBe('avatarUrl');
      expect(enzymeWrapper.find('ButtonComponent').first().props().onClick)
        .toBe(props.goToDashboard);
      expect(enzymeWrapper.find('DetailItem')).toHaveLength(4);
    });

    it('should render error', () => {
      const props = {
        error: 'some weird error',
        id: 1,
        username: 'username',
        avatarUrl: 'avatarUrl',
        htmlUrl: 'htmlUrl',
        goToDashboard: jest.fn(),
      };
      const {enzymeWrapper} = setup(props);

      expect(enzymeWrapper.find('Alert').text()).toContain('some weird error');
    });
  });
});
