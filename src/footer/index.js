import React from 'react';

import {Link} from 'react-router-dom';
import {PageHeader} from 'react-bootstrap';
import './index.css';

function Footer() {
  return (
    <PageHeader className='footer'>
      <Link to='/'>Githubly</Link> <small>always close to our customer</small>
    </PageHeader>
  );
}

export default Footer;
