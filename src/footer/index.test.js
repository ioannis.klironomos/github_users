import React from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {StaticRouter} from 'react-router';

import Footer from './index';

Enzyme.configure({adapter: new Adapter()});

const setup = () => {
  return mount(
    <StaticRouter location='someLocation' context={{}}>
      <Footer />
    </StaticRouter>
  );
};

describe('components', () => {
  describe('Footer', () => {
    it('should render self and subcomponents', () => {
      const enzymeWrapper = setup();

      expect(enzymeWrapper.exists()).toBe(true);
      expect(enzymeWrapper.find('div.footer').exists()).toBe(true);

      const mainLinkProps = enzymeWrapper.find('Link').first().props();
      expect(mainLinkProps.to).toBe('/');
    });
  });
});
