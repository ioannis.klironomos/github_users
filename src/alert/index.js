import React from 'react';
import PropTypes from 'prop-types';

import {Alert} from 'react-bootstrap';
import Button from '../button';

function AlertComponent({
  title = 'Something went wrong',
  message,
  mainAction,
  mainActionLabel,
  hideAction,
  hideActionLabel
}) {
  return (
    <Alert bsStyle='danger'>
      <h4>{title}</h4>
      {message &&
        <p>{message}</p>
      }
      <p>
        <Button flavor='danger' onClick={mainAction}>
          {mainActionLabel}
        </Button>
        {mainAction && hideAction &&
          <span> or </span>
        }
        <Button onClick={hideAction}>
          {hideActionLabel}
        </Button>
      </p>
    </Alert>
  );
}

AlertComponent.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
  mainAction: PropTypes.func,
  mainActionLabel: PropTypes.string,
  hideAction: PropTypes.func,
  hideActionLabel: PropTypes.string
};

AlertComponent.defaultProps = {
  title: 'Something is wrong',
  message: 'For some serious reason',
  mainActionLabel: 'Do that',
  hideActionLabel: 'Hide me'
};

export default AlertComponent;
