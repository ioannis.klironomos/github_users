import React from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Alert from './index';

Enzyme.configure({adapter: new Adapter()});

const setup = (withButtons = false) => {
  let props = {
    title: 'title',
    message: 'message'
  };

  if (withButtons) {
    props = {
      ...props,
      mainAction: jest.fn(),
      mainActionLabel: 'mainActionLabel',
      hideAction: jest.fn(),
      hideActionLabel: 'hideActionLabel'
    };
  }

  const enzymeWrapper = mount(<Alert {...props} />);

  return {
    props,
    enzymeWrapper
  };
};

describe('components', () => {
  describe('Alert', () => {
    it('should render self and subcomponents', () => {
      const {enzymeWrapper, props} = setup(true);

      expect(enzymeWrapper.find('h4').text()).toBe('title');
      expect(enzymeWrapper.find('p').first().text()).toBe('message');
      expect(enzymeWrapper.find('span').first().text()).toBe(' or ');

      const mainButtonProps = enzymeWrapper.find('ButtonComponent').first().props();
      expect(mainButtonProps.onClick).toBe(props.mainAction);
      expect(mainButtonProps.children).toBe('mainActionLabel');

      const hideButtonProps = enzymeWrapper.find('ButtonComponent').last().props();
      expect(hideButtonProps.onClick).toBe(props.hideAction);
      expect(hideButtonProps.children).toBe('hideActionLabel');
    });

    it('should call main action on mainButton click', () => {
      const {enzymeWrapper, props} = setup(true);
      expect(props.mainAction.mock.calls.length).toBe(0);

      const mainButton = enzymeWrapper.find('ButtonComponent').first();
      mainButton.props().onClick();
      expect(props.mainAction.mock.calls.length).toBe(1);
    });

    it('should call hide on hideButton click', () => {
      const {enzymeWrapper, props} = setup(true);
      expect(props.hideAction.mock.calls.length).toBe(0);

      const hideButton = enzymeWrapper.find('ButtonComponent').last();
      hideButton.props().onClick();
      expect(props.hideAction.mock.calls.length).toBe(1);
    });

    it('should render self without subcomponent buttons', () => {
      const {enzymeWrapper, props} = setup();

      expect(enzymeWrapper.find('h4').text()).toBe('title');
      expect(enzymeWrapper.find('p').first().text()).toBe('message');

      expect(enzymeWrapper.find('span').first().exists()).toBe(false);

      expect(enzymeWrapper.find('ButtonComponent').first().exists()).toBe(true);
      expect(enzymeWrapper.find('Button').first().exists()).toBe(false);

      expect(enzymeWrapper.find('ButtonComponent').last().exists()).toBe(true);
      expect(enzymeWrapper.find('Button').last().exists()).toBe(false);
    });
  });
});
