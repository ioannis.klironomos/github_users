import React from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {StaticRouter} from 'react-router';

import Header from './index';

Enzyme.configure({adapter: new Adapter()});

const setup = () => {
  return mount(
    <StaticRouter location='someLocation' context={{}}>
      <Header />
    </StaticRouter>
  );
};

describe('components', () => {
  describe('Header', () => {
    it('should render self and subcomponents', () => {
      const enzymeWrapper = setup();

      expect(enzymeWrapper.exists()).toBe(true);
      expect(enzymeWrapper.find('PageHeader').exists()).toBe(true);

      const mainLinkProps = enzymeWrapper.find('Link').first().props();
      expect(mainLinkProps.to).toBe('/');
    });
  });
});
