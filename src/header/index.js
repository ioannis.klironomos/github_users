import React from 'react';

import {Link} from 'react-router-dom';
import {PageHeader} from 'react-bootstrap';

function Header() {
  return (
    <PageHeader>
      <Link to='/'>Githubly</Link> <small>your user search service</small>
    </PageHeader>
  );
}

export default Header;
