import React from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Button from './index';

Enzyme.configure({adapter: new Adapter()});

const setup = (children = null) => {
  let props = {
    flavor: 'primary',
    onClick: jest.fn(),
    label: 'label'
  };

  if (children) {
    props = {
      ...props,
      children
    };
  }

  const enzymeWrapper = mount(<Button {...props} />);

  return {
    props,
    enzymeWrapper
  };
};

describe('components', () => {
  describe('ButtonComponent', () => {
    it('should render self', () => {
      const {enzymeWrapper, props} = setup();
      expect(enzymeWrapper.text()).toBe('label');

      const buttonProps = enzymeWrapper.find('Button').first().props();
      expect(buttonProps.onClick).toBe(props.onClick);
      expect(buttonProps.children).toBe('label');
    });

    it('should render children', () => {
      const {enzymeWrapper, props} = setup('some children');
      expect(enzymeWrapper.text()).toBe('some children');

      const buttonProps = enzymeWrapper.find('Button').first().props();
      expect(buttonProps.children).toBe('some children');
    });

    it('should call main action on mainButton click', () => {
      const {enzymeWrapper, props} = setup();
      expect(props.onClick.mock.calls.length).toBe(0);

      enzymeWrapper.props().onClick();
      expect(props.onClick.mock.calls.length).toBe(1);
    });
  });
});
