import {combineReducers} from 'redux-immutable';
import {routerReducer} from 'react-router-redux';
import usersReducer from './users/reducer';

export default () => {
  return combineReducers({
    router: routerReducer,
    users: usersReducer
  });
};
