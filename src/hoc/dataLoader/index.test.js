import React, {createElement} from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import dataLoader from './index';

Enzyme.configure({adapter: new Adapter()});

const WrappedComponent = () => <div className='wrapped-component' />;
const Wrapper = dataLoader(WrappedComponent);

const setup = (component, props = {}) =>
  mount(createElement(component, {...props}));

describe('components', () => {
  describe('HOC', () => {
    describe('dataLoader', () => {
      it('should return a Wrapper', () => {
        expect(typeof Wrapper).toBe('function');
        expect(Wrapper.displayName).toBe('LoaderHOC(WrappedComponent)')
      });

      describe('Wrapper', () => {
        it('should render self and show loader and wrapped component', () => {
          const props = {
            isDataLoaded: false,
            getData: jest.fn()
          };
          const enzymeWrapper = setup(Wrapper, props);

          expect(enzymeWrapper.exists()).toBe(true);
          expect(props.getData.mock.calls.length).toBe(1);
          expect(enzymeWrapper.find('.loader').exists()).toBe(true);
          expect(enzymeWrapper.find('.wrapped-component').exists()).toBe(true);
        });

        it('should render self without showing loader', () => {
          const props = {
            isDataLoaded: true,
            getData: jest.fn()
          };
          const enzymeWrapper = setup(Wrapper, props);

          expect(enzymeWrapper.exists()).toBe(true);
          expect(props.getData.mock.calls.length).toBe(0);
          expect(enzymeWrapper.find('.loader').exists()).toBe(false);
          expect(enzymeWrapper.find('.wrapped-component').exists()).toBe(true);
        });

        it('should render self and loader, but not wrapped component', () => {
          const Wrapper2 = dataLoader(WrappedComponent, {hideWhileLoading: true});
          const props = {
            isDataLoaded: false,
            getData: jest.fn()
          };
          const enzymeWrapper = setup(Wrapper2, props);

          expect(enzymeWrapper.exists()).toBe(true);
          expect(props.getData.mock.calls.length).toBe(1);
          expect(enzymeWrapper.find('.loader').exists()).toBe(true);
          expect(enzymeWrapper.find('.wrapped-component').exists()).toBe(false);
        });
      });
    });
  });
});
