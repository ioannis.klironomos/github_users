import React from 'react';
import renderElementToTop from '../helpers/renderElementToTop';

import {Switch, Route} from 'react-router-dom';
import {Grid} from 'react-bootstrap';
import Users from '../users';
import User from '../user';
import Header from '../header';
import Footer from '../footer';
import Page404 from '../errorPages/page404';
import './index.css';

function App() {
  return (
    <Grid>
      <div className='wrapper'>
        <Route path='/' component={Header} />
        <Switch>
          <Route exact path='/' component={Users} />
          <Route path='/users/:username' render={renderElementToTop(User)} />
          <Route component={Page404}/>
        </Switch>
        <div className='push' />
      </div>
      <Route path='/' component={Footer} />
    </Grid>
  );
}

export default App;
