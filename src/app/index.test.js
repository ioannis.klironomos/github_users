import React from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {StaticRouter} from 'react-router';

import App from './index';

Enzyme.configure({adapter: new Adapter()});

const setup = () => {
  return mount(
    <StaticRouter location='someLocation' context={{}}>
      <App />
    </StaticRouter>
  );
};

describe('components', () => {
  describe('App', () => {
    it('should render self and subcomponents', () => {
      const enzymeWrapper = setup();

      expect(enzymeWrapper.exists()).toBe(true);
      expect(enzymeWrapper.find('div.wrapper').exists()).toBe(true);
      expect(enzymeWrapper.find('Route').exists()).toBe(true);
    });
  });
});
