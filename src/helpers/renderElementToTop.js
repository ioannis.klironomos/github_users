import {createElement} from 'react';

const scrollToTop = () => window.scrollTo(0, 0);

export default element => props => {
  scrollToTop();
  return createElement(element, {...props});
};
